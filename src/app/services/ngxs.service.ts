import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<AppModels.Deelnemer>('assets/list.json').subscribe((data) => {
      console.log(data);
    });
  }

  // getList() {
  //   return this.getAll().toPromise();
  // }

  getList() {
    return this.getAll();
  }
}


