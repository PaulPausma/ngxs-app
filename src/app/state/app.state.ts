import { State, Action, StateContext, Selector } from '@ngxs/store';

import { mergeMap, catchError, take, tap } from 'rxjs/operators';
import { of } from 'rxjs';

import {
    LogIn, LogOut, DeelnemerOphalen, DeelnemerOphalenFailed
} from './app.actions';

import { TestService } from '../services/ngxs.service';

export const getAppInitialState = (): AppModels.App => ({
    deelnemer: []
});

@State<AppModels.App>({
    name: 'app',
    defaults: getAppInitialState()
})
export class AppState {

    constructor(private testSvc: TestService) { }

    @Selector()
    static getAuthenticated(state: AppModels.AuthStateModel) {
      return state.authenticated;
    }
  
    @Selector()
    static getUser(state: AppModels.AuthStateModel) {
      return state.user;
    }

    @Selector()
    static getDeelnemer(state: AppModels.App) {
        return state.deelnemer;
    }

    // @Selector()
    // static totalCartAmount(state: AppModels.App) {
    //     const priceList = state.cart
    //         .map(c => {
    //             const unitPrice = state.coffeeList.find(x => x.name === c.name).price;  
    //             return unitPrice * c.quantity;
    //         })
    //     const sum = priceList.reduce((acc, curr) => acc + curr, 0);

    //     return sum;
    // }

    // @Selector()
    // static totalCartQuantity(state: AppModels.App) {
    //     const total = state.cart.reduce((acc, curr) => acc + curr.quantity, 0);
    //     return total;
    // }
  
  
    @Action(LogIn)
    logIn({ getState, patchState }: StateContext<AppModels.AuthStateModel>, { payload }: LogIn) {
      const state = getState();
      patchState({
        authenticated: true,
        user: payload
      });
    }
  
    @Action(LogOut)
    logOut({ getState, patchState }: StateContext<AppModels.AuthStateModel>) {
      const state = getState();
      patchState({
        authenticated: false,
        user: null
      });
    }

    @Action(DeelnemerOphalen)
    async getCoffeeList(ctx: StateContext<AppModels.App>, action: DeelnemerOphalen) {
        try {
            const deelnemer = await this.testSvc.getList();
            const state = ctx.getState();
            console.log(deelnemer);

        } catch (error) {
            ctx.dispatch(new DeelnemerOphalenFailed(error))
        }
    }


}