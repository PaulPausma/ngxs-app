import { State, Action, StateContext } from '@ngxs/store';

// /* deelnemer */

// export class GetDeelnemer {
//     static readonly type = '[Pensioenaanvraag] GET_DEELNEMER';
//     constructor() { }
// }

export class LogIn {
  static readonly type = '[Auth] Log in';
  // constructor(public payload: AuthStateModel) {}
  constructor(public payload: string) {}
}

export class LogOut {
  static readonly type = '[Auth] Log out';
  // constructor(public payload: AuthStateModel) {}
  // constructor(public payload: any) {}
}

export class DeelnemerOphalen {
    static readonly type = '[Deelnemer API] GET_DEELNEMER_INFO';
}

export class DeelnemerOphalenSuccess {
    static readonly type = '[Deelnemer API] GET_DEELNEMER_INFO_SUCCESS';
    constructor(public payload: AppModels.Deelnemer[]) { }
}

export class DeelnemerOphalenFailed {
    static readonly type = '[Deelnemer API] GET_DEELNEMER_INFO_FAILED';
    constructor(public payload?: any) { }
}
