namespace AppModels {

    export interface App {
        // define state here
        deelnemer: Deelnemer[];
        // cart: { name: string; quantity: number }[];
    }

    export interface Deelnemer {
        naam: string;
        gebDatum: string;
        burgelijkeStaat: string;
    }
    // export interface Partnergegevens {
    //     name: string;
    //     birthDate: string;
    // }
    // export interface Pensioendatum {
    //     pensioenDate: string;
    // }

    export interface AuthStateModel {
        authenticated: boolean;
        user: any;
      }
    
    
    export interface AppApp {
        readonly app: App;
    }
    
}
