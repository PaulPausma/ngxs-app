import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Store, Select } from '@ngxs/store';
import { LogIn, LogOut, DeelnemerOphalen } from './state/app.actions';
import { AppState } from './state/app.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ngxs-test';

  @Select(AppState.getAuthenticated) authenticated$: Observable<boolean>;
  @Select(AppState.getUser) user$: Observable<boolean>;

  constructor(private store: Store) {}

  ngOnInit() {
    const isListPopulated = this.store.selectSnapshot<AppModels.App>(x => x.app.deelnemer.length);
    if (isListPopulated) { return; }
    this.store.dispatch(new DeelnemerOphalen());
  }

  logIn(userName: string) {
    this.store.dispatch(new LogIn(userName));
  }

  logOut() {
    this.store.dispatch(new LogOut());
  }


}
